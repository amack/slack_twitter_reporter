DO $$ BEGIN
    CREATE TYPE perm_lvl AS ENUM (
        'ban',
        'unranked',
        'voice',
        'driver',
        'mod',
        'admin'
    );
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

CREATE TABLE IF NOT EXISTS users(
        uid text PRIMARY KEY,
        rank perm_lvl DEFAULT 'unranked',
        add_date timestamptz DEFAULT CURRENT_TIMESTAMP
    );


CREATE TABLE IF NOT EXISTS follow(
        id_str text PRIMARY KEY,
        add_usr text NOT NULL,
        add_date timestamptz DEFAULT CURRENT_TIMESTAMP,
        screen_name text NOT NULL,
        picture text DEFAULT 'https://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png',
        watching text[] DEFAULT NULL,
        FOREIGN KEY (add_usr) REFERENCES users (uid)
    );
