# Slack Twitter Reporter
###### A Twitter integration for Slack.

### The files (self-explanitory files ignored):
- /app: Python Sources
- /json: Slack message blocks in json format
- /sql: Database creation scripts
- .style.yapf: Code formatting rules for [yapf](https://github.com/google/yapf).
- Procfile: File used by Heroku to declare proccess types.
- requirements.txt: For installation with pip.
- runtime.txt: File used by heroku to determine Python version.

### Dependencies:
- flask == 1.1.1
- slackclient == 2.0
- slackeventsapi >= 2.1.0
- psycopg2 == 2.8.3 
- python-twitter == 3.5
- APScheduler == 3.6.0
- gunicorn >= 19.9.0
- requests >= 2.22.0

### Requirements:
- Heroku account (if you choose to run on Heroku. Hobby-tier dynos recommended at minimum.)
    - https://devcenter.heroku.com/articles/getting-started-with-python
- Slack workspace and account
    - https://api.slack.com/start/overview
- Twitter API account
    - https://developer.twitter.com/en/docs/basics/getting-started