COMMAND = {
    'default': 0,
    'off': 1,
    'help': 2,
    'about': 3,
    'follow': 4,
    'unfollow': 5,
    'watch': 6,
    'unwatch': 7,
    'rank': 8,
    'promote': 9,
    'demote': 9,
    'ban': 10
}

from sys import stdout

from flask import current_app as app

from .bot import rtmPost

CMD_INVALID = """
            Invalid command. Use the command 'help' to see a list of commands.
            """


def cmdInvalid(channel):
    rtmPost(channel, CMD_INVALID)
    return ('', 200, None)


NOT_IMPLEMENTED = "Command not yet implemented."


def notImplemented(channel):
    rtmPost(channel, NOT_IMPLEMENTED)
    return ('', 200, None)


from re import search

from .blocks import post_block
from .db import sql_insert, sql_select

RANKS = {
    'ban': 0,  #can't do anything
    'unranked': 1,  #can watch and unwatch accounts
    'voice': 2,  #can follow and unfollow accounts they added
    'driver': 3,  #can follow and unfollow all accounts
    'mod': 4,  #can ban and promote users up to driver
    'admin': 5  #can ban and promote users up to admin and restart the app
}

USER_SEL = """SELECT rank FROM users WHERE uid = %s;"""
USER_INS = """INSERT INTO users (uid, rank) VALUES (%s, %s);"""
BADRANK = "You must be rank `{r}` or higher to do that."
CHANGERANK = """UPDATE users SET rank = %s WHERE uid = %s;"""
LOWRANK = "Cannot change rank of user who outranks you."
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"


def cmdHandle(commandstr, infodict):
    '''
    Handle commands from users.
    '''
    user = infodict['user']

    rankstr = 'voice'
    rows = sql_select(USER_SEL, (user, ))
    if rows is None:
        res = sql_insert(USER_INS, (user, rankstr))
        if not res:
            app.logger.warning(
                "Failed to insert user {u} into database.".format(u=user)
            )
    else:
        rankstr = rows[0][0]

    rank = RANKS[rankstr]

    if rank <= RANKS['ban']:
        rtmPost(channel, "Permission denied.")
        return ('', 200, None)

    if 'channel' in infodict.keys():
        channel = infodict['channel']
    else:
        channel = user

    command = commandstr.split(None, 1)
    cmdArgs = None

    cmdStr = command[0].lower()
    if len(command) > 1:
        cmdArgs = command[1]

    if cmdStr not in COMMAND.keys():
        app.logger.debug("Command not found.")
        return cmdInvalid(channel)

    cmd = COMMAND[cmdStr]

    if cmd == COMMAND['off'] and rank == RANKS['admin']:
        app.logger.info("App going down.")
        app.app_context().pop()

    elif cmd == COMMAND['help']:
        app.logger.debug("Handling 'help' command.")
        return post_block(channel, '../json/help.json')

    elif cmd == COMMAND['about']:
        app.logger.debug("Handling 'about' command.")
        return post_block(channel, '../json/about.json')

    elif cmd == COMMAND['follow']:
        app.logger.debug("Handling 'follow' command.")
        return follow(channel, user, cmdArgs, rank)

    elif cmd == COMMAND['unfollow'] and rank >= RANKS['voice']:
        app.logger.debug("Handling 'unfollow' command.")
        return unfollow(channel, user, cmdArgs, rank)

    elif cmd == COMMAND['watch'] and rank >= RANKS['unranked']:
        app.logger.debug("Handling 'watch' command.")
        return watch(channel, user, cmdArgs, rank)

    elif cmd == COMMAND['unwatch']:
        app.logger.debug("Handling 'unwatch' command.")
        return unwatch(channel, user, cmdArgs, rank)

    elif cmd == COMMAND['rank']:
        rtmPost(channel, "You are rank {r}.".format(r=rank))
        return ('', 200, None)

    elif cmd == COMMAND['promote'] or cmd == COMMAND['demote']:
        '''
        This should probably be its own function but whatever.
        '''
        if rank == RANKS['mod'] or rank == RANKS['admin']:
            if cmdArgs is not None:
                change = cmdArgs.split()
                match = search(MENTION_REGEX, change[0])
                if match and change[1] in RANKS.keys():
                    rows = sql_select(USER_SEL, (change[0], ))
                    orrank = None
                    if rows is not None:
                        orrank = rows[0][0]
                        if rank < RANKS[orrank]:
                            rtmPost(channel, LOWRANK)
                            return ('', 200, None)
                    else:
                        rtmPost(channel, "User not found.")
                    res = sql_insert(CHANGERANK, (change[1].change[0]))
                    if res:
                        rtmPost(channel, "Rank changed succesfully.")
                    else:
                        rtmPost(channel, "Could not change rank.")
                else:
                    rtmPost(channel, "Invalid command arguments.")
            else:
                rtmPost(channel, "No arguments given to command.")
        else:
            rtmPost(channel, BADRANK.format(r="mod"))

        return ('', 200, None)

    elif cmd == COMMAND['ban']:
        if rank == RANKS['mod'] or rank == RANKS['admin']:
            if cmdArgs is not None:
                match = search(MENTION_REGEX, cmdArgs)
                if match:
                    rows = sql_select(USER_SEL, (cmdArgs, ))
                    if rows is not None:
                        orrank = rows[0][0]
                        if rank < RANKS[orrank]:
                            rtmPost(channel, LOWRANK)
                            return ('', 200, None)
                        res = sql_insert(CHANGERANK, (cmdArgs, 'ban'))
                        if res:
                            rtmPost(channel, "User banned.")
                        else:
                            rtmPost(channel, "Could not ban user.")
                    else:
                        rtmPost(channel, "User not found.")
                else:
                    rtmPost(channel, "Invalid command argument.")
            else:
                rtmPost(channel, "No arguments given to command.")
        else:
            rtmPost(channel, BADRANK.format(r="mod"))

        return ('', 200, None)

    else:
        app.logger.debug("Handling invalid command.")
        return cmdInvalid(channel)


from twitter import TwitterError

from .twitterdefs import TWT_API, normalize_img

TWT_INS = """INSERT INTO follow (id_str, add_usr, screen_name, picture)
            VALUES (%s, %s, %s, %s) ON CONFLICT (id_str) DO NOTHING;"""

FOLLOW = "Now following Twitter user {t}. To receive notifications whenever this user tweets, use the 'watch' command."


def follow(channel, user, account, rank):

    if rank < RANKS['voice']:
        rtmPost(channel, BADRANK.format(r="voice"))
        return ('', 200, None)

    try:
        twtuser = TWT_API.GetUser(screen_name=account, return_json=True)
        data = (
            twtuser['id_str'], user, twtuser['screen_name'],
            normalize_img(twtuser['profile_image_url_https'])
        )

        res = sql_insert(TWT_INS, data)
        if not res:
            app.logger.warning(
                "Attempt to insert Twitter data into database failed."
            )
            rtmPost(channel, "Could not insert Twitter user into database.")
        else:
            app.logger.debug(
                "Account {t} successfully followed.".format(t=account)
            )
            rtmPost(channel, FOLLOW.format(t=account))

        return ('', 200, None)

    except TwitterError as err:
        app.logger.warning(
            "Twitter Error: {e}".format(e=str(err)), exc_info=True
        )
        rtmPost(channel, "Error receiving information from Twitter.")
        return ('', 200, None)


WATCH_ADD = """SELECT add_usr, watching FROM follow WHERE id_str = %s;"""
NOT_ADD = "You must be rank `driver` or higher to unfollow an account you did not follow. To stop receiving notifications when this account tweets, use the 'unwatch' command instead."
OTHERS_WATCH = "You must be rank driver` to unfollow an account others are watching. To stop receiving notifications when this account tweets, use the 'unwatch' command instead."
UNFOLLOW = """DELETE FROM follow WHERE id_str = %s"""


def unfollow(channel, user, account, rank):

    if rank < RANKS['voice']:
        rtmPost(channel, BADRANK.format(r="voice"))
        return ('', 200, None)

    try:
        twtuser = TWT_API.GetUser(screen_name=account, return_json=True)
        idstr = twtuser['id_str']

        rows = sql_select(WATCH_ADD, (idstr, ))
        if rows is not None:
            addusr = rows[0][0]
            watchlist = rows[0][1]

            if addusr != user and rank < RANKS['driver']:
                rtmPost(channel, NOT_ADD)
                return ('', 200, None)

            if watchlist is not None:
                for i in watchlist:
                    if i != user and rank < RANKS['driver']:
                        rtmPost(channel, OTHERS_WATCH)
                        return ('', 200, None)

            res = sql_insert(UNFOLLOW, (idstr, ))
            if not res:
                app.logger.warning(
                    "Could not remove account {u} from database.".format(
                        u=idstr
                    )
                )
                rtmPost(
                    channel, "Issue unfollowing account {u}.".format(u=account)
                )
                return ('', 200, None)
            else:
                rtmPost(channel, "Account {a} unfollowed.".format(a=account))
                return ('', 200, None)

        else:
            rtmPost(channel, "No account named '{a}' found.".format(a=account))
            return ('', 200, None)

    except TwitterError as err:
        app.logger.warning("Twitter Error: {e}".format(e=str(err)))
        rtmPost(channel, "Error receiving information from Twitter.")
        return ('', 200, None)


WATCH_LIST = """SELECT watching FROM follow WHERE id_str = %s;"""
NO_FOLLOW = "You cannot watch an account that is not followed. To follow an account, use the `follow` command."
UPDATE_WATCH = "UPDATE follow SET watching = %s where id_str = %s;"
NO_UPDATE = "Could not add user {u} to watchlist for account {a}."


def watch(channel, user, account, rank):

    if rank < RANKS['unranked']:
        rtmPost(channel, BADRANK.format(r="voice"))
        return ('', 200, None)

    try:

        twtuser = TWT_API.GetUser(screen_name=account, return_json=True)
        idstr = twtuser['id_str']

        rows = sql_select(WATCH_LIST, (idstr, ))
        if rows is None:
            if rank < RANKS['voice']:
                rtmPost(channel, NO_FOLLOW)
                return ('', 200, None)
            else:
                '''
                Depending on performance, we may need to remove this follow()
                call and replace it with a modified function called
                follow_watch() that inserts an array for the 'watching' field
                in table 'follow' while also inserting the account. Right now
                it does these in seperate transactions.
                '''
                follow(channel, user, account, rank)
                res = sql_insert(UPDATE_WATCH, ([user], idstr))
                if not res:
                    app.logger.warning(NO_UPDATE.format(u=user, a=account))
                    rtmPost(channel, "Error adding user to watchlist.")
                else:
                    rtmPost(channel, "Now watching {a}.".format(a=account))
                return ('', 200, None)
        else:
            watchlist = rows[0][0]
            if watchlist is None:
                res = sql_insert(UPDATE_WATCH, ([user], idstr))
                if not res:
                    app.logger.warning(NO_UPDATE.format(u=user, a=account))
                    rtmPost(channel, "Error adding user to watchlist.")
            elif user not in watchlist:
                watchlist.append(user)
                res = sql_insert(UPDATE_WATCH, (watchlist, idstr))
                if not res:
                    app.logger.warning(NO_UPDATE.format(u=user, a=account))
                    rtmPost(channel, "Error adding user to watchlist.")
            else:
                rtmPost(
                    channel,
                    "Already in watchlist for Twitter account {a}".format(
                        a=account
                    )
                )

        return ('', 200, None)

    except TwitterError as err:
        app.logger.warning("Twitter Error: {e}".format(e=str(err)))
        rtmPost(channel, "Error accessing Twitter data.")
        return ('', 200, None)


def unwatch(channel, user, account, rank):

    if rank < RANKS['unranked']:
        rtmPost(channel, BADRANK.format(r="voice"))
        return ('', 200, None)

    try:

        twtuser = TWT_API.GetUser(screen_name=account, return_json=True)
        idstr = twtuser['id_str']

        rows = sql_select(WATCH_LIST, (idstr, ))
        if rows is not None:
            watchlist = rows[0][0]
            if user in watchlist:
                watchlist.remove(user)
            rtmPost(
                channel, "Succesfully stopped watching {a}.".format(a=account)
            )
        else:
            rtmPost(channel, "Account {a} not followed.".format(a=account))

        return ('', 200, None)

    except TwitterError as err:
        app.logger.warning("Twitter Error: {e}".format(e=str(err)))
        rtmPost(channel, "Error accessing Twitter data.")
