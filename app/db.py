from os.path import exists
from os import environ
from sys import stderr

from flask import current_app as app
from flask import g
from psycopg2 import connect
from psycopg2 import Error as pgErr
from psycopg2 import Warning as pgWarn


def db_path():
    if 'db' not in g:
        #DBPATH should be set to whatever the environmental variable
        #name that designates the database url is. this is just so
        #that different development stages can use different env
        #variable names
        g.db = environ[environ['DBPATH']]
    return g.db


def sql_connection(db):
    '''
    Create the global postgres db connection.
    '''
    if 'conn' not in g:
        try:
            g.conn = connect(db, sslmode='require')
        except pgErr as err:
            app.logger.error(
                "Error: Could not connect to database. " + str(err) + "\n",
                exc_info=True
            )
        except pgWarn as warn:
            app.logger.warning(str(warn))
        app.logger.info("Connected to database.")

    return g.conn


def sql_select(query, columns):
    '''
    Execute select query with specific data parameters and return the rows
    returned by the query.
    '''
    app.logger.debug("Creating postgres cursor.")
    conn = sql_connection(db_path())
    crs = conn.cursor()
    try:
        app.logger.debug("Executing select query.")
        crs.execute(query, columns)
    except pgErr as err:
        conn.rollback()
        app.logger.error(
            "Query failed.\nError: {e}\nQuery: {q}".format(e=str(err), q=query),
            exc_info=True
        )
    except pgWarn as warn:
        app.logger.warning("Warning: " + str(warn), exc_info=True)

    if crs.rowcount > 0:
        fetch = crs.fetchall()
        app.logger.debug("Closing cursor.")
        crs.close()
        return fetch
    else:
        app.logger.debug("Closing cursor.")
        crs.close()
        return None


def simple_select(query):
    '''
    Execute select query with no specified data parameters.
    '''
    app.logger.debug("Creating postgres cursor.")
    conn = sql_connection(db_path())
    crs = conn.cursor()
    try:
        app.logger.debug("Executing select query.")
        crs.execute(query)
    except pgErr as err:
        conn.rollback()
        app.logger.error(
            "Query failed.\nError: {e}\nQuery: {q}".format(e=str(err), q=query),
            exc_info=True
        )
    except pgWarn as warn:
        app.logger.warning("Warning: " + str(warn), exc_info=True)

    if crs.rowcount > 0:
        fetch = crs.fetchall()
        app.logger.debug("Closing cursor.")
        crs.close()
        return fetch
    else:
        app.logger.debug("Closing cursor.")
        crs.close()
        return None


SUCCESS = True
FAIL = False


def sql_insert(query, columns):
    '''
    Execute query to insert data into a database. Returns True on success, and
    False otherwise.
    '''
    app.logger.debug("Opening postgres cursor.")
    conn = sql_connection(db_path())
    crs = conn.cursor()
    try:
        app.logger.debug("Executing insert or update query.")
        crs.execute(query, columns)
        conn.commit()
    except pgErr as err:
        conn.rollback()
        app.logger.error(
            "Query failed.\nError: {e}\nQuery: {q}".format(e=str(err), q=query),
            exc_info=True
        )
        return FAIL
    except pgWarn as warn:
        app.logger.warning("Warning: " + str(warn), exc_info=True)

    app.logger.debug("Closing cursor.")
    crs.close()

    return SUCCESS


def sql_connection_close(e=None):
    '''
    Close db connection. Run on app teardown.
    '''
    if 'conn' in g:
        conn = g.pop('conn', None)

        if conn is not None:
            app.logger.info("Closing database.")
            conn.close()


from os.path import join
from re import match

from flask.cli import with_appcontext
from click import command
from twitter import TwitterError

from .twitterdefs import TWT_API, normalize_img


@command('makedb')
@with_appcontext
def makedb():

    app.logger.info("Begining database creation process.")
    sqlpath = join(app.instance_path, "sql/create_db.sql")
    conn = sql_connection(db_path())

    try:
        query = ""
        with open(sqlpath) as sqlfile:
            query = sqlfile.read()
            sqlfile.close()

        crs = conn.cursor()
        crs.execute(query)
        conn.commit()
        crs.close()

    except OSError as err:
        app.logger.error(str(err), exc_info=True)
        conn.rollback()
        return 0

    except pgErr as err:
        app.logger.error(
            "Database error: {e}".format(e=str(err)), exc_info=True
        )
        conn.rollback()
        return 0

    except pgWarn as warn:
        app.logger.warning("Database warning: {w}".format(w=str(warn)))

    sid = ""
    rows = sql_select("SELECT uid FROM users WHERE rank = %s;", ("admin", ))
    if rows is None:
        sid = input("Enter admin's Slack ID: ")
        while match("^(|[WU].+?)", sid) is None:
            sid = input("Invalid Slack ID. Please enter a valid Slack ID: ")

        ins = """INSERT INTO users (uid, rank) VALUES (%s, %s);"""

        res = sql_insert(ins, (sid, "admin"))
        if not res:
            app.logger.warning(
                "Query unsuccesfully executed: {i}".format(i=ins)
            )
            app.logger.info("Stopping.")
            return 0
        else:
            app.logger.info("Admin user created.")
    else:
        sid = rows[0][0]

    rows = simple_select("SELECT id_str FROM follow;")
    if rows is None:
        try:
            twtusr = TWT_API.GetUser(
                screen_name="ReporterSlack", return_json=True
            )
            ins = """INSERT INTO follow (id_str, add_usr, screen_name, picture)
                    VALUES (%s, %s, %s, %s);"""
            pic = twtusr['profile_image_url_https']
            res = sql_insert(
                ins, (twtusr['id_str'], sid, twtusr['screen_name'], pic)
            )
            if not res:
                app.logger.warning(
                    "Query unsuccesfully executed: {i}".format(i=ins)
                )

        except TwitterError as err:
            app.logger.error(
                "Error requesting Twitter data: {e}".format(e=str(err))
            )
    return 0


def init_db(flaskapp):
    '''
    App configuration and initialization
    '''
    flaskapp.cli.add_command(makedb)
    flaskapp.teardown_appcontext(sql_connection_close)
