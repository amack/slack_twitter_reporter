#!/usr/bin/python

from os import environ

CONSUMER_KEY = environ['TWT_CONSUMER_KEY']
CONSUMER_SECRET = environ['TWT_CONSUMER_SECRET']
ACCESS_TOKEN = environ['TWT_ACCESS_TOKEN']
ACCESS_SECRET = environ['TWT_ACCESS_TOKEN_SECRET']

from time import sleep

from twitter import Api as twitterapi

TWT_API = twitterapi(
    CONSUMER_KEY,
    CONSUMER_SECRET,
    ACCESS_TOKEN,
    ACCESS_SECRET,
    sleep_on_rate_limit=True
)


def normalize_img(url):
    parts = url.rpartition('.')
    normalurl = parts[0] + "_normal" + parts[1] + parts[2]
    return normalurl
