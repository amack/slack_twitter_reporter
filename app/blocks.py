from json import loads
from os.path import join

from flask import current_app as app


def load_block(blockpath):
    '''
    Load a json object containing the block message for slack.
    '''
    block = None
    app.logger.debug("Trying to open json block file.")
    try:
        with app.open_resource(blockpath, 'r') as blockfile:
            blockstr = str(blockfile.read())
            #app.logger.debug("Block: {b}".format(b=blockstr))
            block = loads(blockstr)
            blockfile.close()
    except OSError as err:
        strerr = str(err)
        app.logger.error(
            'Could not load {f}: {s}'.format(f=err.filename, s=strerr),
            exc_info=True
        )

    app.logger.debug("Block loaded.")
    return block


from .bot import client


def block_post(channel, block, private=False, user=None):
    '''
    Post a block message to slack.
    '''
    res = None
    if private and user is not None:
        res = client().chat_postEphemeral(
            channel=channel, user=user, blocks=block
        )
        app.logger.debug("Posting ephemeral block.")
    else:
        res = client().chat_postMessage(
            channel=channel, text="Could not display content.", blocks=block
        )
        app.logger.debug("Posting block.")

    if res is None:
        app.logger.warning("Slack did not respond to api call.")
        return None

    if res is not None and 'ok' in res.data.keys():
        if not res['ok']:
            app.logger.warning(
                "Slack client api call returned error {e}.".format(
                    e=res['error']
                )
            )
        else:
            if 'warning' in res.data.keys():
                app.logger.warning(
                    "Slack client api call returned warning {w}.".format(
                        w=res['warning']
                    )
                )
            else:
                app.logger.debug(
                    "Block successfully sent to channel {c}.".format(c=channel)
                )
        return res['ok']
    else:
        app.logger.warning(
            "Slack did not send a response containing an 'ok' field."
        )
        return None


from .bot import rtmPost


def post_block(channel, path):
    '''
    Post a block message.
    '''
    loadedblock = load_block(path)

    if loadedblock is None:
        rtmPost(channel, "Could not load help message.")
        app.logger.warning("Could not load help block.")
    else:
        app.logger.debug("Block succesfully loaded.")
        block_post(channel, loadedblock)

    return ('', 200, None)
