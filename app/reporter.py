#!/usr/bin/python

from json import loads
import sys
from time import process_time_ns as ns

from twitter import TwitterError
from apscheduler.schedulers.blocking import BlockingScheduler

from .twitterdefs import TWT_API
from .db import simple_select, sql_connection_close
from .blocks import load_block, block_post
from .bot import rtmPost

from flask import current_app as app


class TweetKeyError(KeyError):
    '''
    Used when twitter sends unrecognized keys in json
    '''
    pass


sched = BlockingScheduler()


ACCT_SEL = """SELECT id_str, picture, watching FROM follow;"""

def reporter(channel):
    time1 = ns()
    app.logger.debug(time1)
    rows = simple_select(ACCT_SEL)
    if rows is None:
        app.logger.info("Job scheduled, but no accounts are followed.")
        return 0

    accounts = []
    pics = {}  #dict for profile picture urls
    watchers = {}
    for i in rows:
        accounts.append(i[0])
        watchers[i[0]] = i[2]

    if len(accounts) == 0:
        app.logger.info("Job scheduled, but no accounts are followed.")
        return 0
    
    try:
        stay_alive = 0
        for tweet in TWT_API.GetStreamFilter(follow=accounts):
            if tweet is None:
                stay_alive += 1
                if stay_alive >= 3:
                    app.logger.info("Three stay-alive signals received.")
                    return 0
                else:
                    continue

            #app.logger.debug(tweet)
            tuid, scrname = None, None
            if "user" in tweet.keys():
                tuid = tweet["user"]["id_str"]
                scrname = tweet["user"]["screen_name"]
            else:
                tuid = tweet["tweet"]["user"]["id_str"]
                scrname = tweet["tweet"]["user"]["screen_name"]

            tweetid = tweet["id_str"]

            if tuid is not None:
                if tuid not in accounts:
                    del tweet #twitter sends lots of data, freeing up memory = good
                    time2 = ns()
                    #app.logger.debug(time2-time1)
                    if time2 - time1 >= 6000000000:
                        app.logger.debug("Restarting reporter.")
                        sched.resume()
                        return 0
                    else:
                        continue
            else:
                app.logger.warning(
                        "Could not find user id in Tweet {i}.".format(
                            i=tweetid))
                app.logger.debug(tweet)
                continue

            if scrname is not None:
                notify = "New tweet from @{n}. ".format(
                    n=tweet["user"]["screen_name"]
                )
            else:
                app.logger.warning(
                        "Could not find screen name in Tweet {i}.".format(
                            i=tweetid))
                app.logger.debug(tweet)
                continue
                
            tweeturl = "https://twitter.com/{u}/status/{i}".format(
                u=tweet["user"]["screen_name"], i=tweetid
            )
            '''
            When formatting blocks, seeing things like obj["text"]["text"] with
            two fields of the same name is not a mistake.
            '''
            if "retweeted_status" in tweet.keys() or\
                "quoted_status" in tweet.keys():

                block = load_block("../json/retweet.json")
                if block is None:
                    app.logger.warning("No block loaded, returning.")
                    return 0

                orusr = None
                ortweet = None
                if "quoted_status" in tweet.keys():
                    orusr = tweet["quoted_status"]["user"]["screen_name"]
                    ortweet = tweet["quoted_status"]["text"]
                elif "retweeted_status" in tweet.keys():
                    orusr = tweet["retweeted_status"]["user"]["screen_name"]
                    ortweet = tweet["retweeted_status"]["text"]
                else:
                    raise TweetKeyError()

                block[0]["image_url"] = tweet["user"]["profile_image_url"]
                block[1]["text"]["text"] = block[1]["text"]["text"].format(
                    uname=tweet["user"]["name"]
                )
                block[2]["elements"][0]["text"] = block[2]["elements"][0][
                    "text"].format(scrname=tuid)
                #block[3] is a divider, nothing to format
                block[4]["text"]["text"] = block[4]["text"]["text"].format(
                    tweet=tweet["text"], orname=orusr, ortweet=ortweet
                )
                block[5]["elements"][0]["text"] = block[5]["elements"][0][
                    "text"].format(tstamp=tweet["created_at"])
                #block[6] is a divider, nothing to format
                block[7]["text"]["text"] = block[7]["text"]["text"].format(
                    url=tweeturl
                )
                #app.logger.debug(block)
                
                '''Twitter sends a lot of data, so using del to (kind of) free
                up memory is good. This is the earliest we can do it.'''
                del tweet
                
                block_post(channel, block)
                
                if watchers[tuid] is not None:
                    for user in watchers[tuid]:
                        notify += "<@{u}> ".format(u=user)
                    rtmPost(channel, notify)
                
                time2 = ns()
                #app.logger.debug(time2 - time1)
                if time2 - time1 >= 6000000000: #6000000000 ~ 15 min
                    app.logger.debug("Restarting reporter.")
                    sched.resume()
                    return 0

            else:
                block = load_block("../json/tweet.json")
                if block is None:
                    app.logger.warning("No block loaded, returning.")
                    return 0

                block[0]["image_url"] = block[0]["image_url"].format(
                    imgurl=tweet["user"]["profile_image_url"]
                )
                block[1]["text"]["text"] = block[1]["text"]["text"].format(
                    uname=tweet["user"]["name"]
                )
                block[2]["elements"][0]["text"] = block[2]["elements"][0][
                    "text"].format(scrname=tweet["user"]["screen_name"])
                #block[3] is a divider, nothing to format
                block[4]["text"]["text"] = block[4]["text"]["text"].format(
                    tweet=tweet["text"]
                )
                block[5]["elements"][0]["text"] = block[5]["elements"][0][
                    "text"].format(tstamp=tweet["created_at"])
                #block[6] is a divider, nothing to format
                block[7]["text"]["text"] = block[7]["text"]["text"].format(
                    url=tweeturl
                )

                #app.logger.debug(block)
                
                '''Twitter sends a lot of data, so using del to (kind of) free
                up memory is good. This is the earliest we can do it.'''
                del tweet

                block_post(channel, block)
                
                if watchers[tuid] is not None:
                    for user in watchers[tuid]:
                        notify += "<@{u}> ".format(u=user)
                    rtmPost(channel, notify)
                
                time2 = ns()
                #app.logger.debug(time2 - time1)
                if time2 - time1 >= 6000000000: #6000000000 ~ 15 min
                    app.logger.debug("Restarting reporter.")
                    sched.resume()
                    return 0

    except TwitterError as err:
        app.logger.error("Twitter Error: {e}".format(e=str(err)), exc_info=True)
        return 1

    except TweetKeyError as err:
        app.logger.error(
            "'retweeted_status' or 'quoted_status' no longer in json.",
            exc_info=True
        )

    except ConnectionResetError as err:
        app.logger.warning("Connection reset by peer.")

    except Exception as err:
        app.logger.error(str(err), exc_info=True)

    finally:
        sched.resume()
        sql_connection_close()

    return 0


from .bot import botid, client

from flask import Flask
from flask.cli import with_appcontext


def reportapp():

    app = Flask(__name__)
    app.teardown_appcontext(sql_connection_close)

    return app


@sched.scheduled_job('interval', minutes=1, max_instances=1)
def start_reporting():
    sched.pause()
    app = reportapp()
    with app.app_context():
        id = botid()
        resp = client().users_conversations(
            user=id, types="public_channel,private_channel"
        )

        if "ok" in resp.data.keys():
            if "error" in resp.data.keys():
                app.logger.warning(
                    "Slack returned error '{e}'.".format(e=resp["error"])
                )
                return 0
            elif "warning" in resp.data.keys():
                app.logger.warning(resp["warning"])

            chans = resp["channels"]
            res = 0
            for channel in chans:
                res = reporter(channel["id"])
                if res != 0:
                    app.logger.warning(
                        "Reporter function failed with code {c}.".format(c=res)
                    )
            sched.resume()

        else:
            app.logger.warning("Slack response did not contain 'ok' field.")
            sched.resume()

    return 0


from click import command
from flask.cli import with_appcontext

from .db import sql_connection


@command('report')
@with_appcontext
def schedule_reporting():

    app.logger.info("Starting reporter job.")

    try:
        #sched.add_job(start_reporting, 'interval', minutes=1, max_instances=1)
        sched.start()

    except SystemExit as err:
        '''This can happen with server restarts, etc so its ok despit being an
        exception'''
        sql_connection.close()
        app.logger.error("SystemExit received. Code: {c}".format(c=err.code))

    except KeyboardInterrupt:
        sql_connection.close()
        app.logger.error("Keyboard Interrupt received.")


def init_reporter(flaskapp):
    flaskapp.cli.add_command(schedule_reporting)
