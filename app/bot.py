from getpass import getpass
from time import sleep
from os import environ

from flask import current_app as app
from flask import g
#from slackclient import SlackClient
from slack import WebClient

from .apperror import AuthNotOKError

ONLINE_STR = "Online and listening."


def token():
    '''
    Store the Slack API token.
    '''
    if 'token' not in g:
        g.token = environ['API_TOKEN']
    return g.token


def client():
    '''
    Store the Slack client connection.
    '''
    if 'client' not in g:
        g.client = WebClient(token())
    return g.client


def botid():
    '''
    Store the bot user's ID.
    '''
    #if client().server.connected is True and 'botid' not in g:
    if 'botid' not in g:
        #auth = client().api_call("auth.test")
        auth = client().auth_test()
        if auth['ok']:
            g.botid = auth['user_id']
        else:
            raise AuthNotOKError(
                "Could not get botid because auth.test did not return ok!"
            )

    return g.botid


def ephemeralPost(channel, message, user):
    res = client().chat_postEphemeral(
        channel=channel, user=user, text=message, as_user=botid()
    )
    return res


def publicPost(channel, message):
    res = client().chat_postMessage(channel=channel, text=message)
    return res


def rtmPost(channel, message, private=False, user=None):
    '''
    Send a basic post to Slack.
    '''
    res = None

    if private and user is not None:
        res = ephemeralPost(channel, message, user)
    else:
        res = publicPost(channel, message)

    if res is None:
        app.logger.warning("Slack did not send a response to api call.")

    if res is not None and 'ok' in res.data.keys():
        if not res['ok']:
            app.logger.warning(
                "Slack client api call returned error {e}.".format(
                    e=res['error']
                )
            )
        else:
            if 'warning' in res.data.keys():
                app.logger.warning(
                    "Slack client api call returned warning {w}.".format(
                        w=res['warning']
                    )
                )
            else:
                app.logger.debug(
                    "Message {m} sent to channel {c}.".format(
                        m=message, c=channel
                    )
                )
    else:
        app.logger.warning(
            "Slack did not send a response containing an 'ok' field."
        )

    return res


from datetime import datetime


def schedulePost(channel, day, time, message):
    timestamp = datetime.combine(day, time)

    res = client().chat_scheduleMessage(
        channel=channel, post_at=timestamp.timestamp(), text=message
    )

    if res is None:
        app.logger.warning("Slack did not send a response to api call.")

    if 'ok' in res.data.keys():
        if not res['ok']:
            app.logger.warning(
                "Slack api call returned error {e}.".format(e=res['error'])
            )
            return None
        else:
            if 'warning' in res.data.keys():
                app.logger.warning(
                    "Slack api call returned warning {w}.".format(
                        w=res['warning']
                    )
                )
            else:
                app.logger.debug(
                    "Scheduled message {m} sent to channel {c}.".format(
                        m=message, c=channel
                    )
                )
            if 'scheduled_message_id' in res.data.keys():
                return res['scheduled_message_id']
            else:
                return None
    else:
        app.logger.warning(
            "Slack did not send a response containing an 'ok' field."
        )
        return None


from .cmdhandle import cmdHandle, COMMAND


def chatlistener(event):
    '''
    Listen for posts on Slack and deal with them appropriately. This is
    returned by the slack events adapter defined in the function in 
    __init__.py.
    '''
    command, infodict = parseEvent(event)
    if command is not None:
        app.logger.debug("Command '{c}' received.".format(c=command))
        return cmdHandle(command, infodict)
    else:
        app.logger.debug("No command received.")
        return ('', 404, None)


from re import search
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"


def parseMention(text):
    '''
    Parse text for a slack mention.
    '''
    matches = search(MENTION_REGEX, text)
    if matches is not None:
        app.logger.debug("Matched mention regex.")
        return matches.group(1), matches.group(2)
    else:
        app.logger.debug("Did not match mention regex.")
        return None, None


def parseEvent(event):
    '''
    Parse the event information received from Slack.
    '''
    userid = None
    infodict = {}
    if 'user' in event.keys():
        infodict['user'] = event['user']
    if 'channel' in event.keys():
        infodict['channel'] = event['channel']
    if 'text' in event.keys():
        userid, message = parseMention(event['text'])

        if userid == botid():
            return message, infodict
    return None, None
